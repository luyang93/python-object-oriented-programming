class MyFirstClass(object):
    pass


def main() -> None:
    a = MyFirstClass()
    b = MyFirstClass()
    print(a, b)
    print(a is b)


if __name__ == '__main__':
    main()
