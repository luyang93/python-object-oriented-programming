import math


class Point(object):
    def reset(self) -> None:
        self.x = 0.0
        self.y = 0.0

    def move(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

    def calculate_distance(self, other: "Point") -> float:
        return math.hypot(self.x - other.x, self.y - other.y)


def main() -> None:
    point1 = Point()
    point2 = Point()
    point1.reset()
    point2.move(5, 0)
    print(point2.calculate_distance(point1))
    assert point2.calculate_distance(point1) == point1.calculate_distance(point2)

    point1.move(3, 4)
    print(point1.calculate_distance(point2))
    print(point1.calculate_distance(point1))


if __name__ == '__main__':
    main()
