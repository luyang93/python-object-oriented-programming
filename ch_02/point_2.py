import math


class Point(object):
    """
    Represents a point in two-dimensional geometric coordinates

    >>> p_0 = Point()
    >>> p_1 = Point(3, 4)
    >>> p_0.calculate_distance(p_1)
    5.0
    """

    def __init__(self, x: float = 0, y: float = 0) -> None:
        """
        Initialize the position of a new point. The x and y
        coordinates can be specified. If they are not, the
        point defaults to the origin.

        :param x: float x-coordinate
        :param y: float x-coordinate
        """
        self.move(x, y)

    def reset(self) -> None:
        self.x = 0.0
        self.y = 0.0

    def move(self, x: float, y: float) -> None:
        """
        Move the point to a new location in 2D space.

        :param x: float x-coordinate
        :param y: float x-coordinate
        """
        self.x = x
        self.y = y

    def calculate_distance(self, other: "Point") -> float:
        """
        Calculate the Euclidean distance from this point
        to a second point passed as a parameter.

        :param other: Point instance
        :return: float distance
        """
        return math.hypot(self.x - other.x, self.y - other.y)


def main() -> None:
    point1 = Point()
    point2 = Point()
    point1.reset()
    point2.move(5, 0)
    print(point2.calculate_distance(point1))
    assert point2.calculate_distance(point1) == point1.calculate_distance(point2)

    point1.move(3, 4)
    print(point1.calculate_distance(point2))
    print(point1.calculate_distance(point1))


if __name__ == '__main__':
    main()
