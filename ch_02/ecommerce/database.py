from typing import Optional


class Database:
    """The Database Implementation"""

    def __init__(self, connection: Optional[str] = None) -> None:
        """Create a connection to a database"""
        pass


db: Optional[Database] = None


def initialize_database(connection: Optional[str] = None) -> None:
    """Initialize the database"""
    global db
    db = Database(connection)


def get_database(connection: Optional[str] = None) -> Database:
    """Get the database"""
    global db
    if not db:
        db = Database(connection)
    return db
